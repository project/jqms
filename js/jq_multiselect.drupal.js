/**
 * @file
 * Attaches behaviors for the JQ Multiselect module.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches behavior to the page from defined settings for JQ Multiselect to each specified element.
   */
  Drupal.behaviors.jq_multiselect = {

    settings: {

      /**
       * Completely ignores elements that match one of these selectors.
       *
       * Disabled on:
       * - Field UI
       * - WYSIWYG elements
       * - Tabledrag weights
       * - Elements that have opted-out of Chosen
       * - Elements already processed by Chosen.
       *
       * @type {string}
       */
      ignoreSelector: '#field-ui-field-storage-add-form select, #entity-form-display-edit-form select, #entity-view-display-edit-form select, .wysiwyg, .draggable select[name$="[weight]"], .draggable select[name$="[position]"], .locale-translate-filter-form select, .chosen-disable, .chosen-processed',

      /**
       * Explicit "opt-in" selector.
       *
       * @type {string}
       */
      optedInSelector: 'select.jq-enable',

      /**
       * The default selector, overridden by drupalSettings.
       *
       * @type {string}
       */
      selector: 'select:visible'
    },
    attach: function (context, settings) {
      this.settings = this.getSettings(settings);
      if (typeof drupalSettings.module !== 'undefined') {
        var coloumns = drupalSettings.module.coloumns;
        var options = drupalSettings.module.options;
        var placeholder = drupalSettings.module.placeholder;
        this.getElements(context).once('jq_multiselect').each(function (index, element) {
          this.createMultiselect(element, coloumns, options, placeholder);
        }.bind(this));
      }
    },

    /**
     * Creates a JQ Multiselect instance for a specific element.
     *
     * @param {jQuery|HTMLElement} element
     *   The element.
     */
    createMultiselect: function (element, coloumns, options, placeholder) {
      var $element = $(element);
      if ($element.attr('multiple')) {
        $element.multiselect({
          columns: coloumns,
          search: options.search,
          selectAll: options.select_all,
          texts: {
            placeholder: placeholder,
            search: 'Search States'
          }
        });
      }
    },

    /**
     * Retrieves the elements that should be converted into JQ instances.
     *
     * @param {jQuery|Element} context
     *   A DOM Element, Document, or jQuery object to use as context.
     * @param {string} [selector]
     *   A selector to use, defaults to the default selector in the settings.
     */
    getElements: function (context, selector) {
      var $context = $(context || document);
      var $elements = $context.find(selector || this.settings.selector);

      // Remove elements that should not be converted into Chosen.
      $elements = $elements.filter(function (i, element) {
        return this.filterElements(element);
      }.bind(this));

      // Add elements that have explicitly opted in to Chosen.
      $elements = $elements.add($context.find(this.settings.optedInSelector));

      return $elements;
    },

    /**
     * Filter out elements that should not be converted into JQ.
     *
     * @param {jQuery|HTMLElement} element
     *   The element.
     *
     * @return {boolean}
     *   TRUE if the element should stay, FALSE otherwise.
     */
    filterElements: function (element) {
      var $element = $(element);

      // Remove elements that should be ignored completely.
      if ($element.is(this.settings.ignoreSelector)) {
        return false;
      }

      // Zero value means no minimum.
      var minOptions = $element.attr('multiple') ? this.settings.minimum_multiple : this.settings.minimum_single;
      return !minOptions || $element.find('option').length >= minOptions;
    },

    /**
     * Retrieves the settings passed from Drupal.
     *
     * @param {Object} [settings]
     *   Passed Drupal settings object, if any.
     */
    getSettings: function (settings) {
      return $.extend(true, {}, this.settings, settings && settings.jq_multiselect || drupalSettings.module);
    }
  };

})(jQuery, Drupal, drupalSettings);
