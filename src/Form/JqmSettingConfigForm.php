<?php

namespace Drupal\jq_multiselect\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class JqmSettingConfigForm.
 */
class JqmSettingConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
	return [
		'jq_multiselect.jqmsettingconfig',
	];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
	return 'jqm_setting_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

	$config = $this->config('jq_multiselect.jqmsettingconfig');
	$form['settings'] = [
		'#type' => 'fieldset',
		'#title' => $this->t('Settings'),
	];

	$form['settings']['select_attribute'] = [
		'#type' => 'textarea',
		'#title' => $this->t('Bind JQ Multiselect with specified Selectors'),
		'#description' => $this->t('A comma-separated list of jQuery selectors to apply JQ Multiselect to, such as select#edit-operation, select#edit-type or .jq-multiselect-select. Defaults to select to apply JQ Multiselect to all select input elements. '),
		'#default_value' => $config->get('select_attribute'),
	];

	$form['settings']['coloumn'] = [
		'#type' => 'select',
		'#title' => $this->t('Coloumn'),
		'#description' => $this->t('Number of coloumns to group the items in Select Listbox.'),
		'#options' => ['1' => $this->t('1'), '2' => $this->t('2'), '3' => $this->t('3'), '4' => $this->t('4'), '5' => $this->t('5')],
		'#default_value' => $config->get('coloumn'),
	];

	$form['settings']['search'] = [
		'#type' => 'radios',
		'#title' => $this->t('Search'),
		'#description' => $this->t('Enable search for items.'),
		'#default_value' => $config->get('search'),
		'#options' => [
			1 => $this
					->t('Yes'),
			0 => $this
					->t('No'),
		],
	];

	$form['settings']['select_all'] = [
		'#type' => 'radios',
		'#title' => $this->t('Select All'),
		'#description' => $this->t('Enable select all items Button.'),
		'#default_value' => $config->get('select_all'),
		'#options' => [
			1 => $this
					->t('Yes'),
			0 => $this
					->t('No'),
		],
	];

	$form['settings']['placeholder'] = [
		'#type' => 'textfield',
		'#title' => $this->t('Placeholder'),
		'#default_value' => $config->get('placeholder'),
	];
	


	
	return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
	parent::submitForm($form, $form_state);

	$this->config('jq_multiselect.jqmsettingconfig')
	  ->set('select_attribute', $form_state->getValue('select_attribute'))
	  ->set('coloumn', $form_state->getValue('coloumn'))
	  ->set('search', $form_state->getValue('search'))
	  ->set('select_all', $form_state->getValue('select_all'))
	  ->set('placeholder', $form_state->getValue('placeholder'))
	  ->save();

  }

}
